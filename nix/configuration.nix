{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.extraModprobeConfig = ''
    options snd-hda-intel model=dell-headset-multi
  '';

  networking.hostName = "tim-nixos"; # Define your hostname.
  networking.networkmanager.enable = true;
  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Asia/Vladivostok";

  environment.pathsToLink = [ "/share" ];

  environment.systemPackages = with pkgs; [
    acpi 
    vim
    haskellPackages.xmonad-contrib
    haskellPackages.xmonad-extras
    haskellPackages.xmonad
    haskellPackages.xmobar
    iftop
    docker
    docker_compose
    tdesktop
    fish
    git
    fontconfig
    udevil
    unzip
    chromium
    xorg.xbacklight
    spotify
    stalonetray
    slack
    smplayer
    pcmanfm
    vscode
    yubikey-personalization
    seafile-client
    gnupg
    lsof
    usbutils
    awscli
    dbeaver
    transmission-remote-gtk
    inkscape
    qpdfview
    shared_mime_info
  ];

  fonts.fonts = with pkgs; [
    fira-code
    fira-code-symbols
    inconsolata
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.fish.enable = true;

  # List services that you want to enable:

  services.devmon.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.udev.packages = with pkgs; [
    yubikey-personalization
  ];

  virtualisation.docker.enable = true;
  virtualisation.docker.extraOptions = "--userns-remap=\"tim:dockremap\"";

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 19000 19001 5001 5432 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  hardware.bluetooth.enable = true;

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us,ru";
    xkbOptions = "grp:caps_toggle";
    libinput.enable = true;
    multitouch.invertScroll = true;

    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      extraPackages = haskellPackages: [
       	haskellPackages.xmonad-contrib
        haskellPackages.xmonad-extras
#        haskellPackages.xmonad
	haskellPackages.xmobar
      ];
    };
    windowManager.default = "xmonad";
  };
  
  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.extraUsers.guest = {
  #   isNormalUser = true;
  #   uid = 1000;
  # };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
  users.groups.dockremap.gid = 10000;

  users.extraUsers = {
    tim = {
      isNormalUser = true;
      home = "/home/tim";
      uid = 1000;
      description = "Tim Tonks";
      extraGroups = [ "wheel" "audio" "docker" "dockremap" "networkmanager" ];
      shell = pkgs.fish;
      subUidRanges = [
	{ startUid = 1000; count = 1; }
        { startUid = 100000; count = 65536; }
      ];
     # subGidRanges = [
     #   { startGid = 100000; count = 65536; }
     # ];
    };
    dockremap = {
      isSystemUser = true;
      uid = 10000;
      group = "dockremap";
     # subUidRanges = [
     #   { startUid = 100000; count = 65536; }
     # ];
     subGidRanges = [
       { startGid = 131; count = 1; }
       { startGid = 100000; count = 65536; }
     ];
    };
  };
 
  hardware.opengl.extraPackages = [
    pkgs.vaapiIntel
  ];
 
  nixpkgs.config = {
    allowUnfree = true;

    chromium = {
      enablePepperFlash = true;
      enablePepperPDF = true;
    };
  };
}
