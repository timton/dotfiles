import XMonad

import XMonad.Util.Run
import XMonad.Util.EZConfig
import XMonad.Util.CustomKeys
import XMonad.Util.SpawnOnce

import XMonad.Config.Desktop

import XMonad.Layout.NoBorders 
import XMonad.Layout.BorderResize 
import XMonad.Layout.MouseResizableTile 
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps

import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import System.IO

import qualified XMonad.StackSet as W
import qualified Data.Map as M

myTerminal = "urxvt"
myModMask = mod4Mask

myWorkspaces = clickable $ ["1:main","2:chrome","3:ff","4:code","5:msg"] ++ map show [6..9]
	where 
		clickable l = zipWith clickHandler [1..] l
		clickHandler idx label = "<action=`xdotool key super+" ++ show(idx) ++ "`>" ++ label ++ "</action>"

myAdditionalKeys =
	[ ( "<XF86MonBrightnessUp>",    spawn "xbacklight +10%" )	
	, ( "<XF86MonBrightnessDown>",  spawn "xbacklight -10%" )	
	, ( "<XF86AudioMute>",          spawn "amixer -q set Master toggle" )
	, ( "<XF86AudioLowerVolume>",   spawn "amixer sset Master 5%-" )
	, ( "<XF86AudioRaiseVolume>",   spawn "amixer sset Master 5%+" )
	]

myKeys = customKeys delKeys addKeys 
	where
		delKeys :: XConfig l -> [(KeyMask, KeySym)]
		delKeys XConfig { modMask = modm } = []

		addKeys :: XConfig l -> [((KeyMask, KeySym), X ())]
		addKeys XConfig { modMask = modm } =	
			[ ( (modm, xK_c), spawn "chromium" )
			]

myManageHook = composeAll
	[ className =? "Firefox" --> moveToWorkspace 3
	, appName =? "chromium-browser" --> moveToWorkspace 2
	, appName =? "telegram-desktop" --> moveToWorkspace 5
	, appName =? "slack" --> moveToWorkspace 5
	, appName =? "code" --> moveToWorkspace 4
	, className =? "Spotify" --> moveToWorkspace 6
	, className =? "stalonetray" --> doIgnore
	, manageDocks
	]
		where moveToWorkspace i = doF (W.shift $ myWorkspaces !! (i-1)) 

myLayoutHook = smartBorders $ avoidStruts $ tiledResizable ||| Full
	where
		tall = Tall nmaster delta ratio
		gaps' = gaps [(U,1), (D,10), (L,10), (R,10)]
		tiled = gaps' $ spacing 10 $ tall
		tiledResizable = gaps' $ mouseResizableTile
		tiledResizableMirrored = gaps' $ mouseResizableTileMirrored

		nmaster = 1
		ratio = 1/2
		delta = 3/100

myXmobar xmproc = def
	{ ppOutput = hPutStrLn xmproc
	, ppOrder = \(ws:l:t:_) -> [ws]
	, ppCurrent = xmobarColor "#69DFFA" "" . wrap "[" "]"
	, ppTitle = xmobarColor "#69DFFA" "" . shorten 32
	, ppLayout = \_ -> ""
	}

myLogHook = dynamicLogWithPP . myXmobar

myStartupHook = do 
	setWMName "LG3D"
	spawnOnce "stalonetray -c ~/.stalonetrayrc"
--	spawnOnce "xset r rate 200 100"

myConfig xmproc = def
	{ workspaces = myWorkspaces
	, startupHook = myStartupHook
	,	modMask = myModMask
	, keys = myKeys
	, manageHook = myManageHook
	, layoutHook = myLayoutHook
	, handleEventHook = handleEventHook defaultConfig <+> docksEventHook
	, logHook = myLogHook xmproc
	} `additionalKeysP` myAdditionalKeys

main = do 
  xmproc <- spawnPipe "xmobar"
  xmonad $ ewmh $ myConfig xmproc 
